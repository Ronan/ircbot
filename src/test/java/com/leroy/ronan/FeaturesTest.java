package com.leroy.ronan;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(
        monochrome=true,
        strict=true
      )
public class FeaturesTest {

}
