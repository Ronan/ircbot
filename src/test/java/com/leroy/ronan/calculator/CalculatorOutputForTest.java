package com.leroy.ronan.calculator;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import com.leroy.ronan.calculator.message.CalculatorMessage;

public class CalculatorOutputForTest implements Consumer<CalculatorMessage>{

    private List<CalculatorMessage> received;
    
    public CalculatorOutputForTest() {
        super();
        this.received = new ArrayList<>();
    }

    @Override
    public void accept(CalculatorMessage message) {
        received.add(message);
    }
    
    public int size() {
        return received.size();
    }

    public CalculatorMessage get(int index) {
        return received.get(index);
    }
}
