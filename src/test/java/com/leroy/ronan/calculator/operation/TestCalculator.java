package com.leroy.ronan.calculator.operation;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

import org.junit.Assert;
import org.junit.Test;

import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.message.CalculatorMessageToOne;

public class TestCalculator {

	@Test
	public void test() {
	    List<String> response = new ArrayList<>();
        Consumer<CalculatorMessage> output = new Consumer<CalculatorMessage>() {
            @Override
            public void accept(CalculatorMessage msg) {
                response.add(msg.getContent());
            }
        };
        CalculatorOperation operator = new CalculatorOperation("operator", output, Operation.ADDITION);
	    
        CalculatorMessage msg = CalculatorMessageToOne.build("test", "1+1");
        operator.accept(msg);
        
        Assert.assertEquals(1, response.size());
        Assert.assertEquals("1+1=2.0", response.get(0));
	}
}
