package com.leroy.ronan.calculator.operation;

import org.junit.Assert;
import org.junit.Test;

import com.leroy.ronan.calculator.operation.Operation;

public class TestAdditionner {

	private static final double DELTA = 0.0000000001;
	
	@Test
	public void testAdd() {
		Assert.assertEquals(2.0, Operation.ADDITION.compute(1.0, 1.0), DELTA);
	}
	
}
