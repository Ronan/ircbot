package com.leroy.ronan.calculator.repository;

import java.util.Collection;

public class MemoryRepositoryForTest<K, T extends RepositoryData<K>> extends MemoryRepository<K, T>{

    public int size() {
        return dataMap.size();
    }

    public Collection<T> getPendingQueries() {
        return dataMap.values();
    }

}
