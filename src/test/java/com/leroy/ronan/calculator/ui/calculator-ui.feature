Feature: Calculator User Interface
Calculator user interface is receiving messages from the user and transmit operation to any splitter.
Calculator user interface is also receiving results messages and transmit the result to the user.

Scenario: Receiving an operation
Given a calculator user interface
 When the calculator user interface receive the operation "1+1" from "me"
 Then the calculator user interface should transmit operation "1+1" to any operation splitter
  And the calculator user interface should be waiting for the result of "1+1" for "me"

Scenario: Receiving a pending result
Given a calculator user interface
  And the calculator user interface is waiting to give to "me" the result of operation with UUID "uuid"
 When the calculator user interface receive the result of the operation "uuid"
 Then the calculator user interface should transmit the result of the operation to "me"
  And the calculator user interface should not be waiting for the result of operation with UUID "uuid"

Scenario: Receiving a unknown result
Given a calculator user interface
 When the calculator user interface receive the result of the operation "uuid"
 Then the calculator user interface should transmit nothing
  And the calculator user interface should not be waiting for the result of operation with UUID "uuid"
