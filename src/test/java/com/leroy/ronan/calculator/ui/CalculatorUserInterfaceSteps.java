package com.leroy.ronan.calculator.ui;

import org.junit.Assert;
import org.mockito.Mockito;

import com.leroy.ronan.calculator.CalculatorBuilder;
import com.leroy.ronan.calculator.CalculatorOutputForTest;
import com.leroy.ronan.calculator.CalculatorRecipientList;
import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.message.CalculatorMessageToAny;
import com.leroy.ronan.calculator.message.CalculatorMessageToOne;
import com.leroy.ronan.calculator.repository.MemoryRepositoryForTest;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class CalculatorUserInterfaceSteps {

    private CalculatorOutputForTest output = new CalculatorOutputForTest();
    private MemoryRepositoryForTest<String, CalculatorUIQuery> memory = new MemoryRepositoryForTest<>();
    private CalculatorUserInterface ui;

    @Given("^a calculator user interface$")
    public void a_calculator_user_interface() throws Throwable {
        ui = new CalculatorBuilder()
                .havingOutput(output)
                .havingMemory(memory)
                .build();
    }

    @Given("^the calculator user interface is waiting to give to \"([^\"]*)\" the result of operation with UUID \"([^\"]*)\"$")
    public void the_calculator_user_interface_is_waiting_to_give_to_the_result_of_operation_with_UUID(String dest, String uuid) throws Throwable {
        CalculatorUIQuery query = Mockito.mock(CalculatorUIQuery.class);
        Mockito.when(query.getKey()).thenReturn(uuid);
        Mockito.when(query.getFrom()).thenReturn(dest);
        
        memory.put(query);
    }


    @When("^the calculator user interface receive the operation \"([^\"]*)\" from \"([^\"]*)\"$")
    public void the_calculator_user_interface_receive_the_operation_from(String operation, String from) throws Throwable {
        CalculatorMessage message = CalculatorMessageToOne.build(ui.getName(), operation);
        message.setFrom(from);
        ui.accept(message);
    }
    
    @When("^the calculator user interface receive the result of the operation \"([^\"]*)\"$")
    public void the_calculator_user_interface_receive_the_result_of_the_operation(String uuid) throws Throwable {
        CalculatorMessage message = CalculatorMessageToOne.build(ui.getName(), uuid+"=1");
        ui.accept(message);
    }

    @Then("^the calculator user interface should transmit operation \"([^\"]*)\" to any operation splitter$")
    public void the_calculator_user_interface_should_transmit_operation_to_any_operation_splitter(String operation) throws Throwable {
        Assert.assertEquals(1, output.size());
        CalculatorMessage expected = CalculatorMessageToAny.build(CalculatorRecipientList.SPLITTER, operation);
        Assert.assertEquals(expected.getTo(), output.get(0).getTo());
        String actual = output.get(0).getContent().split("=")[1];
        Assert.assertEquals(expected.getContent(), actual);
    }

    @Then("^the calculator user interface should be waiting for the result of \"([^\"]*)\" for \"([^\"]*)\"$")
    public void the_calculator_user_interface_should_be_waiting_for_the_result_of_for(String operation, String user) throws Throwable {
        Assert.assertEquals(1, memory.size());
        CalculatorUIQuery query = memory.getPendingQueries().stream().findAny().get();
        Assert.assertEquals(user, query.getFrom());
        Assert.assertEquals(operation, query.getOperation());
    }

    @Then("^the calculator user interface should transmit the result of the operation to \"([^\"]*)\"$")
    public void the_calculator_user_interface_should_transmit_the_result_of_the_operation_to(String user) throws Throwable {
        Assert.assertEquals(1, output.size());
        CalculatorMessage expected = CalculatorMessageToOne.build(user, "something");
        Assert.assertEquals(expected.getTo(), output.get(0).getTo());
    }

    @Then("^the calculator user interface should not be waiting for the result of operation with UUID \"([^\"]*)\"$")
    public void the_calculator_user_interface_should_not_be_waiting_for_the_result_of_operation_with_UUID(String uuid) throws Throwable {
        Assert.assertEquals(0, memory.size());
    }

    @Then("^the calculator user interface should transmit nothing$")
    public void the_calculator_user_interface_should_transmit_nothing() throws Throwable {
        Assert.assertEquals(0, output.size());
    }
}
