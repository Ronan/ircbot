package com.leroy.ronan.irc;

import static com.leroy.ronan.irc.config.Channel.USER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.junit.Assert;
import org.junit.Test;

import com.leroy.ronan.irc.config.Host;

public class TestTimeBot {

    @Test
    public void test() throws NickAlreadyInUseException, IOException, IrcException, InterruptedException{
        List<String> response = new ArrayList<>();

        IrcBot timebot = new TimeBot(Host.host, USER.getChannel(), "timebot");
        IrcBot mybot = new LambdaBot(Host.host, USER.getChannel(), "testtimebot", msg -> response.add(msg.getContent()));
        
        timebot.connect();
        mybot.connect();
        
        while(!timebot.isReady() && !mybot.isReady()) {
            Thread.sleep(1000);
        }

        Thread.sleep(5000);
        mybot.sendMessage(timebot.getName(), "time");
        Thread.sleep(5000);
        
        Assert.assertTrue(response.size() > 0);
    }
}
