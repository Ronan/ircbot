package com.leroy.ronan.irc;

import static com.leroy.ronan.irc.config.Channel.RESULTS;
import static com.leroy.ronan.irc.config.Channel.SPLITTERS;
import static com.leroy.ronan.irc.config.Channel.USER;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Stream;

import org.jibble.pircbot.IrcException;
import org.jibble.pircbot.NickAlreadyInUseException;
import org.junit.Assert;
import org.junit.Test;

import com.leroy.ronan.irc.config.Host;

public class TestCalculatorBot {

    @Test
    public void calculatorbot_should_answer_when_receiving_message() throws NickAlreadyInUseException, IOException, IrcException, InterruptedException{
        List<String> userMessages = new ArrayList<>();
        List<String> splitterMessages = new ArrayList<>();

        IrcBot uiBot       = new CalculatorUIBot(Host.host, "uiBot", USER.getChannel(), SPLITTERS.getChannel(), RESULTS.getChannel());
        IrcBot userBot     = new LambdaBot(Host.host, USER.getChannel(), "userBot", msg -> userMessages.add(msg.getContent()));
        IrcBot splitterBot = new LambdaBot(Host.host, SPLITTERS.getChannel(), "splitterBot", msg -> splitterMessages.add(msg.getContent()));

        IrcBot[] bots = {uiBot, userBot, splitterBot};
        Stream.of(bots).forEach(bot -> bot.connect());

        while(Stream.of(bots).anyMatch(bot -> !bot.isReady())) {
            Thread.sleep(1000);
        }
        
        Thread.sleep(5000);
        userBot.sendMessage(uiBot.getName(), "1+1");
        Thread.sleep(5000);
        
        Assert.assertTrue(splitterMessages.size() > 0);
        
        String message = splitterMessages.get(0);
        String uuid = message.split("=")[0];
        splitterBot.sendMessage(RESULTS.getChannel(), uuid+"=2");
        Thread.sleep(5000);
        
        Assert.assertTrue(userMessages.size() > 0);
    }
}
