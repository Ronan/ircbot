Feature: Irc Bot Messaging features.
The IRC bot should be able to send messaging to :
   - 1 other bot by name.
   - 1 other bot by list.
   - Any other bot by list.

Background:
Given a mocked user bot is created
Given a user interface bot is created
Given 3 splitter bots are created
Given 2 additionner bots are created
Given all those bots are connected and ready

Scenario: user interface sends a message to mocked user
When calculator sends a message to user
Then user receives a message
 And the calculator does not receive any message
 And the splitters do not receive any message
 And the adders do not receive any message

Scenario: user interface sends a message to any in splitter list
When calculator sends a message to any in splitter list
Then user does not receive any message
 And the calculator does not receive any message
 And a splitter receives a message
 And the adders do not receive any message

Scenario: adder sends a message to all in result list
When the first adder sends a message to all in result list
Then user does not receive any message
 And the calculator receives a message
 And the splitters do not receive any message
 And all the adders but the first one receive a message
