package com.leroy.ronan.irc.messaging;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import org.junit.Assert;

import com.leroy.ronan.irc.CalculatorBotType;
import com.leroy.ronan.irc.IrcBot;
import com.leroy.ronan.irc.LambdaBot;
import com.leroy.ronan.irc.Message;
import com.leroy.ronan.irc.config.Channel;
import com.leroy.ronan.irc.config.Host;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class IrcMessagingSteps {
    
    private static IrcBot userBot = null;
    private static IrcBot uiBot = null;
    private static List<IrcBot> splitBots = null;
    private static List<IrcBot> addBots = null;
    private static Map<String, Message> receivedMessages = new HashMap<>();
    
    @Given("^a mocked user bot is created$")
    public void a_mocked_user_bot_is_created() throws Throwable {
        if (userBot == null) {
            userBot = new LambdaBot(Host.host, Channel.USER.getChannel(), "user", msg -> receivedMessages.put("user", msg));
        }
    }

    @Given("^a user interface bot is created$")
    public void a_user_interface_bot_is_created() throws Throwable {
        if (uiBot == null) {
            uiBot = buildTestBot(CalculatorBotType.INTERFACE, 0);
        }
    }

    @Given("^(\\d+) splitter bots are created$")
    public void splitter_bots_are_created(int nb) throws Throwable {
        if (splitBots == null) {
            splitBots = IntStream.range(0, nb)
                    .mapToObj(i -> buildTestBot(CalculatorBotType.SPLITTER, i))
                    .collect(Collectors.toList());
        }
    }

    @Given("^(\\d+) additionner bots are created$")
    public void additionner_bots_are_created(int nb) throws Throwable {
        if (addBots == null) {
            addBots = IntStream.range(0, nb)
                    .mapToObj(i -> buildTestBot(CalculatorBotType.ADDER, i))
                    .collect(Collectors.toList());
        }
    }

    @Given("^all those bots are connected and ready$")
    public void all_those_bots_are_connected_and_ready() throws Throwable {
        receivedMessages.clear();
        getAllBots().stream()
            .filter(bot -> !bot.isReady())
            .forEach(bot -> bot.connect());
        while(getAllBots().stream().anyMatch(bot -> !bot.isReady())) {
            Thread.sleep(1000);
        }
    }

    @When("^calculator sends a message to user$")
    public void calculator_sends_a_message_to_user() throws Throwable {
        uiBot.sendMessageTo(userBot.getName(), "message");
        Thread.sleep(1000);
    }

    @When("^calculator sends a message to any in splitter list$")
    public void calculator_sends_a_message_to_any_in_splitter_list() throws Throwable {
        uiBot.sendMessageToAny(CalculatorBotType.SPLITTER.getMainChannel(), CalculatorBotType.SPLITTER.getPrefix(), "message");
        Thread.sleep(1000);
    }

    @When("^the first adder sends a message to all in result list$")
    public void the_first_adder_sends_a_message_to_all_in_result_list() throws Throwable {
        addBots.get(0).sendMessageToAll(Channel.RESULTS, "message");
        Thread.sleep(1000);
    }

    @Then("^user receives a message$")
    public void user_receives_a_message() throws Throwable {
        Assert.assertTrue(receivedMessages.containsKey(userBot.getName()));
    }

    @Then("^user does not receive any message$")
    public void user_does_not_receive_any_message() throws Throwable {
        Assert.assertFalse(receivedMessages.containsKey(userBot.getName()));
    }

    @Then("^the calculator does not receive any message$")
    public void the_calculator_does_not_receive_any_message() throws Throwable {
        Assert.assertFalse(receivedMessages.containsKey(uiBot.getName()));
    }

    @Then("^the calculator receives a message$")
    public void the_calculator_receives_a_message() throws Throwable {
        Assert.assertTrue(receivedMessages.containsKey(uiBot.getName()));
    }
    
    @Then("^the splitters do not receive any message$")
    public void the_splitters_do_not_receive_any_message() throws Throwable {
        splitBots.stream()
            .forEach(bot -> Assert.assertFalse(receivedMessages.containsKey(bot.getName())));
    }

    @Then("^a splitter receives a message$")
    public void a_splitter_receives_a_message() throws Throwable {
        Assert.assertEquals(
                1, 
                splitBots.stream()
                    .filter(bot -> receivedMessages.containsKey(bot.getName()))
                    .count());
    }
    
    @Then("^the adders do not receive any message$")
    public void the_adders_do_not_receive_any_message() throws Throwable {
        addBots.stream()
            .forEach(bot -> Assert.assertFalse(receivedMessages.containsKey(bot.getName())));
    }

    @Then("^all the adders but the first one receive a message$")
    public void all_the_adders_but_the_first_one_receive_a_message() throws Throwable {
        Assert.assertEquals(
                addBots.size()-1, 
                addBots.stream()
                    .filter(bot -> !bot.getName().equals(addBots.get(0).getName()))
                    .filter(bot -> receivedMessages.containsKey(bot.getName()))
                    .count());
    }

    private IrcBot buildTestBot(CalculatorBotType type, int index){
        String name = type.getIrcName(String.valueOf(index));
        String[] channels = type.getChannels().stream().map(Channel::getChannel).toArray(size -> new String[size]);
        return new LambdaBot(Host.host, channels, name, msg -> receivedMessages.put(name, msg));
    }
    
    private Collection<IrcBot> getAllBots() {
        List<IrcBot> bots = new ArrayList<>();
        bots.add(userBot);
        bots.add(uiBot);
        bots.addAll(splitBots);
        bots.addAll(addBots);
        return bots;
    }
}
