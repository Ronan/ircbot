package com.leroy.ronan.irc;

import static com.leroy.ronan.irc.config.Channel.RESULTS;
import static com.leroy.ronan.irc.config.Channel.SPLITTERS;
import static com.leroy.ronan.irc.config.Channel.USER;

import org.apache.commons.lang3.mutable.Mutable;
import org.apache.commons.lang3.mutable.MutableObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.leroy.ronan.calculator.CalculatorBuilder;
import com.leroy.ronan.calculator.operation.CalculatorOperation;
import com.leroy.ronan.calculator.operation.Operation;
import com.leroy.ronan.irc.config.Host;

public class TestCalculatorBotAdditionnerConsume {

	private String name;
	private CalculatorUIBot bot;
	final Mutable<String> response = new MutableObject<>("");

    @Before
    public void init(){
    	name = "testcalcbot"; 
    	bot = new CalculatorUIBot(Host.host, "uiBot", USER.getChannel(), SPLITTERS.getChannel(), RESULTS.getChannel());
    }

    @Test
    public void should_be_able_to_consume_2_operand_addition() {
    	bot.consume(new Message("me", name+":1+1"));
    	Assert.assertEquals("1+1="+Double.valueOf(2), response.getValue());
    }

    @Test
    public void should_be_able_to_consume_2_operand_floating_point_addition() {
    	bot.consume(new Message("me", name+":0.123+123.0"));
    	Assert.assertEquals("0.123+123.0=123.123", response.getValue());
    }

    @Test
    public void should_be_able_to_consume_2_operand_multiplication() {
    	bot.consume(new Message("me", name+":1*1"));
    	Assert.assertEquals("1*1=?", response.getValue());
    }

    @Test
    public void should_be_able_to_consume_2_operand_addition_for_other() {
    	bot.consume(new Message("me", "me:1+1"));
    	Assert.assertEquals("1+1="+Double.valueOf(2), response.getValue());
    }

    @Test
    public void should_be_able_to_consume_3_operand_addition() {
    	bot.consume(new Message("me", "me:1+1+1"));
    	Assert.assertEquals("1+1+1=?", response.getValue());
    }
}
