package com.leroy.ronan.calculator.ui;

import java.util.function.Consumer;

import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.message.CalculatorMessageToOne;
import com.leroy.ronan.calculator.repository.Repository;

public class CalculatorUIMessageResult extends CalculatorUIMessage {

    private String uuid;
    private String result;
    
    protected CalculatorUIMessageResult(String content) {
        String[] contentArray = content.split("=");
        uuid = contentArray[0];
        result = contentArray[1];
    }

    @Override
    protected void consume(Repository<String, CalculatorUIQuery> repository, Consumer<CalculatorMessage> ouput) {
        CalculatorUIQuery query = repository.get(this.uuid);
        if (query != null) {
            CalculatorMessage message = CalculatorMessageToOne.build(query.getFrom(), query.getOperation()+"="+result);
            ouput.accept(message);
        }
        repository.remove(this.uuid);
    }

}
