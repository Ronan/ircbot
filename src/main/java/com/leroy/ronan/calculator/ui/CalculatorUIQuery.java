package com.leroy.ronan.calculator.ui;

import com.leroy.ronan.calculator.repository.RepositoryData;

public class CalculatorUIQuery implements RepositoryData<String>{

    private String key;
    private String from;
    private String operation;
    
    protected CalculatorUIQuery(String key, String from, String operation) {
        super();
        this.key = key;
        this.from = from;
        this.operation = operation;
    }

    @Override
    public String getKey() {
        return this.key;
    }

    protected String getFrom() {
        return this.from;
    }

    protected String getOperation() {
        return this.operation;
    }
}
