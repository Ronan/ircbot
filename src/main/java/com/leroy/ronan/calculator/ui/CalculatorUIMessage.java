package com.leroy.ronan.calculator.ui;

import java.util.function.Consumer;

import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.repository.Repository;

public abstract class CalculatorUIMessage {

    public static CalculatorUIMessage from(CalculatorMessage message) {
        CalculatorUIMessage uiMsg = null;
        if (message.getContent().contains("=")){
            uiMsg = new CalculatorUIMessageResult(message.getContent());
        }else {
            uiMsg = new CalculatorUIMessageQuery(message.getFrom(), message.getContent());
        }
        return uiMsg;
    }
    
    protected abstract void consume(Repository<String, CalculatorUIQuery> repository, Consumer<CalculatorMessage> ouput);
}
