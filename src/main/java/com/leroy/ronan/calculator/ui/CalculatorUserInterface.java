package com.leroy.ronan.calculator.ui;

import java.util.function.Consumer;

import com.leroy.ronan.calculator.CalculatorService;
import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.repository.Repository;

public class CalculatorUserInterface extends CalculatorService {

    private Repository<String, CalculatorUIQuery> memory;
    
    public CalculatorUserInterface(String id, Consumer<CalculatorMessage> output, Repository<String, CalculatorUIQuery> memory) {
        super(id, output);
        this.memory = memory;
    }

    @Override
    public void accept(CalculatorMessage message) {
        CalculatorUIMessage uiMsg = CalculatorUIMessage.from(message);
        uiMsg.consume(memory, this::send);
    }

}
