package com.leroy.ronan.calculator.ui;

import java.util.UUID;
import java.util.function.Consumer;

import com.leroy.ronan.calculator.CalculatorRecipientList;
import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.message.CalculatorMessageToAny;
import com.leroy.ronan.calculator.repository.Repository;

public class CalculatorUIMessageQuery extends CalculatorUIMessage {

    private String from;
    private String content;
    
    protected CalculatorUIMessageQuery(String from, String content) {
        super();
        this.from = from;
        this.content = content;
    }

    @Override
    protected void consume(Repository<String, CalculatorUIQuery> repository, Consumer<CalculatorMessage> ouput) {
        String uuid = UUID.randomUUID().toString();
        CalculatorUIQuery query = new CalculatorUIQuery(uuid, this.from, this.content);
        repository.put(query);
        
        CalculatorMessage message = CalculatorMessageToAny.build(CalculatorRecipientList.SPLITTER, uuid+"="+content);
        ouput.accept(message);
    }

}
