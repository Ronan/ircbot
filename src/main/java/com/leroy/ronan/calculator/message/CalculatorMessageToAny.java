package com.leroy.ronan.calculator.message;

import com.leroy.ronan.calculator.CalculatorRecipientList;

public class CalculatorMessageToAny extends CalculatorMessage {

    public static CalculatorMessageToAny build(CalculatorRecipientList list, String content) {
        return new CalculatorMessageToAny(list, content);
    }
    
    protected CalculatorMessageToAny(CalculatorRecipientList list, String content) {
        super(list.name(), content);
    }

}
