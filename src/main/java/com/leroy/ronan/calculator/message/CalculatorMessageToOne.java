package com.leroy.ronan.calculator.message;

public class CalculatorMessageToOne extends CalculatorMessage {

    public static CalculatorMessageToOne build(String recipient, String content) {
        return new CalculatorMessageToOne(recipient, content);
    }
    
    protected CalculatorMessageToOne(String recipient, String content) {
        super(recipient, content);
    }
}
