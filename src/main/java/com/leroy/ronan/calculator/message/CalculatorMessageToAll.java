package com.leroy.ronan.calculator.message;

import com.leroy.ronan.calculator.CalculatorRecipientList;

public class CalculatorMessageToAll extends CalculatorMessage {

    public static CalculatorMessageToAll build(CalculatorRecipientList list, String content) {
        return new CalculatorMessageToAll(list, content);
    }
    
    protected CalculatorMessageToAll(CalculatorRecipientList list, String content) {
        super(list.name(), content);
    }

}
