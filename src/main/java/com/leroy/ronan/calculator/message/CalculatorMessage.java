package com.leroy.ronan.calculator.message;

public class CalculatorMessage {

    private String to;
    private String from;
    private String content;
    
    protected CalculatorMessage(String to, String content) {
        super();
        this.to = to;
        this.content = content;
    }
    
    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getContent() {
        return content;
    }

    public String getTo() {
        return to;
    }
    
}
