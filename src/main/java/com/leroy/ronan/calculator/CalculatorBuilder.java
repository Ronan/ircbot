package com.leroy.ronan.calculator;

import java.util.function.Consumer;

import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.repository.Repository;
import com.leroy.ronan.calculator.ui.CalculatorUIQuery;
import com.leroy.ronan.calculator.ui.CalculatorUserInterface;

public class CalculatorBuilder {

    private String name;
    private Consumer<CalculatorMessage> output;
    private Repository<String, CalculatorUIQuery> memory;
    
    public CalculatorBuilder havingName(String name) {
        this.name = name;
        return this;
    }

    public CalculatorBuilder havingOutput(Consumer<CalculatorMessage> output) {
        this.output = output;
        return this;
    }

    public CalculatorBuilder havingMemory(Repository<String, CalculatorUIQuery> memory) {
        this.memory = memory;
        return this;
    }

    public CalculatorUserInterface build() {
        CalculatorUserInterface ui = new CalculatorUserInterface(name, output, memory);
        return ui;
    }

}
