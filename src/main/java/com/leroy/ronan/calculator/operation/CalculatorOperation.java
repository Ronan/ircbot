package com.leroy.ronan.calculator.operation;

import java.util.function.Consumer;

import com.leroy.ronan.calculator.CalculatorRecipientList;
import com.leroy.ronan.calculator.CalculatorService;
import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.message.CalculatorMessageToAll;

public class CalculatorOperation extends CalculatorService{

    private Operation operator;
    
    public CalculatorOperation(String id, Consumer<CalculatorMessage> output, Operation operator) {
        super(id, output);
        this.operator = operator;
    }
    
    @Override
    public void accept(CalculatorMessage msg) {
        String input = msg.getContent();
        String response = input + "=";
        try {
            int index = input.indexOf(operator.getSymbol());
            Double d1 = Double.valueOf(input.substring(0, index));
            Double d2 = Double.valueOf(input.substring(index+1));
            Double result = operator.compute(d1, d2);
            response += result;
        }catch(Exception e) {
            response += '?';
        }
        this.send(CalculatorMessageToAll.build(CalculatorRecipientList.RESULT, response));
    }
}
