package com.leroy.ronan.calculator.operation;

import java.util.function.BiFunction;

public enum Operation {
	
	ADDITION('+', (d1, d2) -> d1+d2);
	
	private char symbol;
	private BiFunction<Double, Double, Double> operation;
	
	private Operation(char symbol, BiFunction<Double, Double, Double> operation) {
		this.symbol = symbol;
		this.operation = operation;
	}

	public char getSymbol() {
		return symbol;
	}
	public double compute(double d1, double d2) {
		return operation.apply(d1, d2);
	}
}
