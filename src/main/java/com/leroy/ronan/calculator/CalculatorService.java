package com.leroy.ronan.calculator;

import java.util.function.Consumer;

import com.leroy.ronan.calculator.message.CalculatorMessage;

public abstract class CalculatorService implements Consumer<CalculatorMessage>{

    private String id;
    private Consumer<CalculatorMessage> output;
    
    public CalculatorService(String id, Consumer<CalculatorMessage> output) {
        super();
        this.id = id;
        this.output = output;
    }
    
    protected void send(CalculatorMessage msg) {
        msg.setFrom(this.id);
        output.accept(msg);
    }
    
    public String getName(){
        return this.id;
    }
    
    @Override
    public abstract void accept(CalculatorMessage msg);
    
}
