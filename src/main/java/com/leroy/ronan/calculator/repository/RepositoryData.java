package com.leroy.ronan.calculator.repository;

public interface RepositoryData<K> {

    K getKey();
    
}
