package com.leroy.ronan.calculator.repository;

public interface Repository<K, T extends RepositoryData<K>> {

    void put(T data);
    T get(K key);
    void remove(K key);

}
