package com.leroy.ronan.calculator.repository;

import java.util.HashMap;
import java.util.Map;

public class MemoryRepository<K, T extends RepositoryData<K>> implements Repository<K, T> {

    protected Map<K, T> dataMap;
    
    public MemoryRepository(){
        dataMap = new HashMap<>();
    }
    
    @Override
    public void put(T data) {
        dataMap.put(data.getKey(), data);
    }

    @Override
    public T get(K key) {
        return dataMap.get(key);
    }

    @Override
    public void remove(K key) {
        dataMap.remove(key);
    }

}
