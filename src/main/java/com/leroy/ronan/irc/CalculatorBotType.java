package com.leroy.ronan.irc;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import com.leroy.ronan.calculator.CalculatorRecipientList;
import com.leroy.ronan.irc.config.Channel;

public enum CalculatorBotType {
    INTERFACE("calcui",  Channel.USER,      new Channel[]{Channel.SPLITTERS, Channel.RESULTS}),
    SPLITTER("splitter", Channel.SPLITTERS, new Channel[]{Channel.ADDITION}),
    ADDER("adder",       Channel.ADDITION,  new Channel[]{Channel.RESULTS}),
    ;

    private String prefix;
    private Channel main;
    private Channel[] secondary;
    
    private CalculatorBotType(String prefix, Channel main, Channel[] secondary) {
        this.prefix = prefix;
        this.main = main;
        this.secondary = secondary;
    }
    
    public String getPrefix() {
        return this.prefix;
    }
    
    public String getIrcName(String name){
        return prefix+"-"+name;
    }

    public Channel getMainChannel() {
        return this.main;
    }

    public List<Channel> getChannels() {
        List<Channel> channels = Arrays.stream(secondary).collect(Collectors.toList());
        channels.add(main);
        return channels;
    }

    public static CalculatorBotType of(String to) {
        CalculatorBotType res = null;
        CalculatorRecipientList list = CalculatorRecipientList.valueOf(to);
        switch(list){
            case SPLITTER:
                res = CalculatorBotType.SPLITTER;
                break;
            case RESULT:
                res = null;
                break;
        }
        return res;
    }
}
