package com.leroy.ronan.irc;

import java.time.LocalDateTime;

public class TimeBot extends IrcBot {
    
    public TimeBot(String host, String channel, String name) {
        super(host, channel, name);
    }

    @Override
    protected void consume(Message msg) {
        if (msg.getContent().equalsIgnoreCase("time")) {
            sendMessage(msg.getFrom(), "The time is now " + LocalDateTime.now().toString());
        }
    }
}
