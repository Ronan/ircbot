package com.leroy.ronan.irc.config;

import com.leroy.ronan.calculator.CalculatorRecipientList;

public enum Channel {
    USER("#calculator"),
    RESULTS("#calcresult"),
    SPLITTERS("#calcsplitter"),
    ADDITION("#calcadd"),
    JAM("#jam_events"),
    ;
    
    private String channel;

    private Channel(String channel) {
        this.channel = channel;
    }

    public String getChannel() {
        return channel;
    }

    public static Channel of(String to) {
        Channel res = null;
        CalculatorRecipientList list = CalculatorRecipientList.valueOf(to);
        switch(list){
            case SPLITTER:
                res = Channel.SPLITTERS;
                break;
            case RESULT:
                res = Channel.RESULTS;
                break;
        }
        return res;
    }
}
