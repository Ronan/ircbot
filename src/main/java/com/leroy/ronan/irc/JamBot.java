package com.leroy.ronan.irc;

import java.util.HashMap;
import java.util.Map;

import com.leroy.ronan.irc.config.Channel;

public class JamBot extends IrcBot{

	public JamBot() {
		super("irc.freenode.net", "#jam_events", "RonanLRJamBot");

	}
	
	private Map<String, Double> map = new HashMap<>();

	@Override
	protected void consume(Message msg) {
		try{
			if ("Calculator".equals(msg.getFrom())) {
				if (msg.getContent().startsWith("[CALCUL]")){
					String operation = msg.getContent().substring("[CALCUL]".length());
					String[] tab = operation.split("\\]\\[");
					String op = tab[0].substring(1);
					String id = tab[1];
					String dt = tab[2].substring(0, tab[2].length()-1);
	
					String[] dataTab = dt.split(";");
					Double data1 = map.get(dataTab[0]);
					if (data1 == null){
						data1 = Double.valueOf(dataTab[0]);
					}
					Double data2 = map.get(dataTab[1]);
					if (data2 == null){
						data2 = Double.valueOf(dataTab[1]);
					}
					Double result = null;
					if ("ADDITION".equals(op)){
						result = data1+data2;
					}
					if ("SOUSTRACTION".equals(op)){
						result = data1-data2;
					}
					if ("MULTIPLICATION".equals(op)){
						result = data1*data2;
					}
					if ("DIVISION".equals(op)){
						result = data1/data2;
					}
					if (result != null) {
						map.put(id, result);
						sendMessageToAll(Channel.JAM, "[RESULTAT]["+id+"]["+result+"]");
					}
				}
			}
		}catch(Exception e){
			// RAS
		}
	}
	
	public static void main(String...args) throws InterruptedException {
		JamBot bot = new JamBot();
		bot.connect();
		while(!bot.isReady()) {
            Thread.sleep(1000);
		}
	}

}
