package com.leroy.ronan.irc;

import java.util.function.Consumer;
import java.util.function.Predicate;

public class LambdaBot extends IrcBot{

    private Consumer<Message> consumer;
    
    public LambdaBot(String host, String channel, String name, Consumer<Message> consumer) {
        this(host, new String[]{channel}, name, consumer);
    }

    public LambdaBot(String host, String[] channels, String name, Consumer<Message> consumer) {
        super(host, channels, name);
        this.consumer = consumer;
    }

    @Override
    protected void consume(Message msg) {
        consumer.accept(msg);
    }
}
