package com.leroy.ronan.irc;

import java.util.stream.Stream;

import org.jibble.pircbot.PircBot;

import com.leroy.ronan.irc.config.Channel;

public abstract class IrcBot extends PircBot {

    private String host;
    private String[] channels;
    
    public IrcBot(String host, String[] channels, String name) {
        super();
        this.host = host;
        this.channels = channels;
        this.setName(name);
    }
    
    public IrcBot(String host, String channel, String name) {
        this(host, new String[]{channel}, name);
    }
    
    public void connect() {
        this.setVerbose(true);
        try {
            this.connect(host);
            Stream.of(channels).forEach(chan -> this.joinChannel(chan));
        }catch(Exception e){
            throw new RuntimeException(e);
        }
    }

    public boolean isConnected(String channel) {
        return this.isConnected() 
            && Stream.of(this.getChannels()).filter(s -> channel.equals(s)).findFirst().isPresent();
    }

    public boolean isReady() {
        return this.isConnected()
            && Stream.of(channels).allMatch(chan -> this.isConnected(chan));
    }

    @Override
    public void onMessage(String channel, String sender, String login, String hostname, String message) {
        consume(new Message(sender, message));
    }
    
    @Override
    public void onPrivateMessage(String sender, String login, String hostname, String message){
        consume(new Message(sender, message));
    }
    
    protected abstract void consume(Message msg);

    public void sendMessageTo(String target, String message) {
        super.sendMessage(target, message);
    }

    public void sendMessageToAll(Channel target, String message) {
        super.sendMessage(target.getChannel(), message);
    }
    
    public void sendMessageToAny(Channel target, String prefix, String message) {
        Stream.of(this.getUsers(target.getChannel()))
            .map(user -> user.getNick())
            .filter(name -> name.startsWith(prefix))
            .findAny()
            .ifPresent(name -> sendMessageTo(name, message));
    }

}
