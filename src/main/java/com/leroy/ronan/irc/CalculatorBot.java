package com.leroy.ronan.irc;

import java.util.function.Consumer;

import com.leroy.ronan.calculator.CalculatorService;
import com.leroy.ronan.calculator.message.CalculatorMessage;
import com.leroy.ronan.calculator.message.CalculatorMessageToAll;
import com.leroy.ronan.calculator.message.CalculatorMessageToAny;
import com.leroy.ronan.calculator.message.CalculatorMessageToOne;
import com.leroy.ronan.irc.config.Channel;

public class CalculatorBot extends IrcBot implements Consumer<CalculatorMessage> {

    private CalculatorService service;
    
    public CalculatorBot(String host, String[] channels, String name) {
        super(host, channels, name);
    }

    public void setService(CalculatorService service) {
        this.service = service;
    }
    
    @Override
    public void accept(CalculatorMessage msg) {
        if (msg instanceof CalculatorMessageToOne){
            sendMessageTo(msg.getTo(), msg.getContent());
        } else if (msg instanceof CalculatorMessageToAny) {
            CalculatorBotType type = CalculatorBotType.of(msg.getTo());
            sendMessageToAny(type.getMainChannel(), type.getPrefix(), msg.getContent());
        } else if (msg instanceof CalculatorMessageToAll) {
            sendMessageToAll(Channel.of(msg.getTo()), msg.getContent());
        }
    }

    @Override
    protected void consume(Message msg) {
        if (this.service != null) {
            CalculatorMessage calcMsg = CalculatorMessageToOne.build(this.service.getName(), msg.getContent());
            calcMsg.setFrom(msg.getFrom());
            service.accept(calcMsg);
        }
    }
}
