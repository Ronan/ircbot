package com.leroy.ronan.irc;

import com.leroy.ronan.calculator.CalculatorBuilder;
import com.leroy.ronan.calculator.repository.MemoryRepository;
import com.leroy.ronan.calculator.ui.CalculatorUserInterface;

public class CalculatorUIBot extends CalculatorBot {

    public CalculatorUIBot(String host, String name, String users, String splitters, String results) {
        super(host, new String[] {users, splitters, results}, name);
        CalculatorUserInterface ui = new CalculatorBuilder()
                        .havingName(name)
                        .havingOutput(this)
                        .havingMemory(new MemoryRepository<>())
                        .build();
        this.setService(ui);
    }
    
}
